[![PyPI status](https://img.shields.io/pypi/status/retailiate-api.svg)](https://pypi.python.org/pypi/retailiate-api/)
[![PyPI version](https://img.shields.io/pypi/v/retailiate-api.svg)](https://pypi.python.org/pypi/retailiate-api/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/retailiate-api.svg)](https://pypi.python.org/pypi/retailiate-api/)
[![Pipeline status](https://gitlab.com/frkl/retailiate-api/badges/develop/pipeline.svg)](https://gitlab.com/frkl/retailiate-api/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# retailiate-api

*Api to manage retailiate*


## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'retailiate-api' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 retailiate_api
    git clone https://gitlab.com/frkl/retailiate_api
    cd <retailiate_api_dir>
    pyenv local retailiate_api
    pip install -e .[all-dev]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
