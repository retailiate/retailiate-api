FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

ENV MODULE_NAME="retailiate_api.app.main"

COPY requirements-docker.txt .
RUN pip install -r requirements-docker.txt

COPY src/retailiate_api /app/retailiate_api
