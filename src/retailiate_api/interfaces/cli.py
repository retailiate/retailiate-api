# -*- coding: utf-8 -*-
import click


@click.command()
@click.pass_context
def cli(ctx):

    print("Hello World!")


if __name__ == "__main__":
    cli()
