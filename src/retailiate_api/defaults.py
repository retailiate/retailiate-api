# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs

retailiate_api_app_dirs = AppDirs("retailiate_api", "frkl")

if not hasattr(sys, "frozen"):
    RETAILIATE_API_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `retailiate_api` module."""
else:
    RETAILIATE_API_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "tings")
    """Marker to indicate the base folder for the `retailiate_api` module."""

RETAILIATE_API_RESOURCES_FOLDER = os.path.join(
    RETAILIATE_API_MODULE_BASE_FOLDER, "resources"
)
