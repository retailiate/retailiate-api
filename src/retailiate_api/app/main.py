# -*- coding: utf-8 -*-
from collections import Sequence

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from retailiate_api.app import retailiate
from retailiate_api.app.api.api_v1 import api_router

app = FastAPI()

# CORS
# Set all CORS enabled origins
if retailiate.config.backend_cors_origins:

    origins = []
    bco = retailiate.config.backend_cors_origins

    if isinstance(bco, str):
        origins_raw = bco.split(",")
    elif isinstance(bco, Sequence):
        origins_raw = origins
    else:
        raise TypeError(
            f"Invalid type for config value 'backend_cors_origins': {type(bco)}"
        )

    for origin in origins_raw:
        use_origin = origin.strip()
        origins.append(use_origin)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(api_router, prefix="/v1")
