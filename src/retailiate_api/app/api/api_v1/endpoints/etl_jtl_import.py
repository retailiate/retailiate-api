# -*- coding: utf-8 -*-

import logging

from datetime import datetime, timezone

from fastapi import APIRouter, File, UploadFile

from retailiate_api.app import retailiate

log = logging.getLogger("retailiate-api")

router = APIRouter()


@router.post("/import")
async def jtl_import(fileb: UploadFile = File(...)):

    # try:
    #     producer = retailiate.get_pulsar_producer(
    #         "jtl_import_producer",
    #         topic="jtl_import_item_added",
    #         schema=AvroSchema(JtlExtractEventSchema),
    #     )
    # except Exception as e:
    #     raise HTTPException(status_code=500, detail=str(e))
    timestamp = datetime.now().replace(tzinfo=timezone.utc).timestamp()
    id = f"jtl_{timestamp}"
    object_name = f"{id}.bz2"

    result = retailiate.upload_to_bucket(
        bucket_name="jtl",
        object_name=object_name,
        upload_file=fileb,
        content_type="application/bzip2",
    )
    print(type(result))

    # data = {"id": id, "storage": "minio", "bucket": "jtl", "object_name": object_name}
    # producer.send(JtlExtractEventSchema(**data))
    return {"process_id": object_name, "storage_details": result}


# class MinioPipe(BasePipe):
#     def __init__(self, filepath, mode, session, endpoint_url, *args, **kwargs):
#
#         self._filepath = filepath
#         self._mode = mode
#         self._sendpoint_url = endpoint_url
#         self._session = session
#
#     def close(self):
#
#         super(MinioPipe, self).close()
#
#
# CONFIG_PIPE_CLASSES = {
#     "file": FilePipe,
#     "stdinout": StdInOutPipe,
#     "gcs": GCSPipe,
#     "minio": MinioPipe,
# }


# def init_modules(runner_config, is_tap):
#     runner_modules = {}
#
#     if is_tap:
#         runner_modules["state_storage"] = init_module(
#             runner_config, CONFIG_STATE_STORAGE_CLASSES, "tap_state"
#         )
#
#     runner_modules["metrics_storage"] = init_module(
#         runner_config, CONFIG_METRICS_STORAGE_CLASSES, "metrics"
#     )
#
#     pipe = init_module(runner_config, CONFIG_PIPE_CLASSES, "pipe")
#     if not pipe:
#         pipe = StdInOutPipe()
#     runner_modules["pipe"] = pipe
#
#     return runner_modules

#
# @router.post("/load")
# async def jtl_load():
#
#     logger = init_logger()
#
#     # target_config = {
#     #     "disable_collection": True,
#     #     "postgres_database": retailiate.config.get("postgres_db_name"),
#     #     "postgres_host": retailiate.config.get("postgres_db_host"),
#     #     "postgrreplicationes_password": retailiate.config.get("postgres_db_password"),
#     #     "postgres_port": 5432,
#     #     "postgres_schema": "public",
#     #     "postgres_username": retailiate.config.get("postgres_db_user")
#     # }
#     target_config = {
#         "disable_collection": True,
#         "postgres_database": "retailiate",
#         "postgres_host": "10.0.0.6",
#         "postgres_password": "RtVNxGexm4t5xh8CAs4AxWy2AelvzuNGNBtokKuFV9Z1U5gacmOanpkywVLyFDMn",
#         "postgres_port": 5432,
#         "postgres_schema": "public",
#         "postgres_username": "retailiate",
#     }
#     try:
#         temp = tempfile.NamedTemporaryFile(mode="w", delete=False)
#         config_path = temp.name
#         json.dump(target_config, temp)
#         temp.close()
#
#         access_key = retailiate.config.minio_access_key
#         secret_key = retailiate.config.minio_secret_key
#         host = retailiate.config.minio_host
#         is_ssl = retailiate.config.minio_is_ssl
#
#         host = "s3.lo.dt.ht"
#         access_key = "minio"
#         secret_key = "minio123"
#         is_ssl = True
#         port = 443 if is_ssl else 80
#
#         file_path = "jtl/jtl_data_small.log.bz2"
#
#         minio_url = f"s3://{access_key}:{secret_key}@{host}:{port}@{file_path}"
#         # transport_params=dict(session=boto3.Session(aws_access_key_id=access_key,aws_secret_access_key=secret_key), resource_kwargs=dict(endpoint_url=f'{proto}://{host}:{port}'))
#
#         runner_config = {
#             "target_config_path": config_path,
#             "target_command": "/home/markus/.pyenv/versions/singer_target_postgres/bin/target-postgres",
#             "pipe": {"type": "file", "options": {"filepath": minio_url, "mode": "rb"}},
#         }
#
#         target_command = runner_config.get("target_command")
#         target_config_path = runner_config.get("target_config_path")
#         runner_modules = init_modules(runner_config, False)
#
#         run_target(
#             logger,
#             target_command,
#             target_config_path=target_config_path,
#             **runner_modules,
#         )
#     finally:
#         os.unlink(config_path)

# access_key = retailiate.config.minio_access_key
# secret_key = retailiate.config.minio_secret_key
# host = retailiate.config.minio_host
# is_ssl = retailiate.config.minio_is_ssl
# port = 443 if is_ssl else 80
# proto = "https" if is_ssl else "http"

# object_name = "jtl_1577695427.381032.bz2"
#
# retailiate.minio_client.fget_object(bucket_name="jtl", object_name=object_name)
#
# logger = init_logger()
# runner_config = {
#     "target_config": {
#         "disable_collection": True,
#         "postgres_database": "retailiate",
#         "postgres_host": "10.0.0.6",
#         "postgres_password": "RtVNxGexm4t5xh8CAs4AxWy2AelvzuNGNBtokKuFV9Z1U5gacmOanpkywVLyFDMn",
#         "postgres_port": 5432,
#         "postgres_schema": "public",
#         "postgres_username": "retailiate"
#     },
#     "target_command": "/home/markus/.pyenv/versions/singer_target_postgres/bin/target-postgres",
#     "pipe": {
#         "type": "file",
#         "options": {
#             "filepath": minio_url,
#             "mode": "rb",
#             "session": session,
#             "endpoint_url": endpoint_url
#         }
#     }
#
# }
#
# target_command = runner_config.get("target_command")
# target_config_path = runner_config.get("target_config_path")
# runner_modules = init_modules(runner_config, False)
#
# run_target(
#     logger,
#     target_command,
#     target_config_path=target_config_path,
#     **runner_modules
# )
#
# os.remove("/tmp/jtl_1577292101.878491")
