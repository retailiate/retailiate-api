# -*- coding: utf-8 -*-
from fastapi import APIRouter

from retailiate_api.app.api.api_v1.endpoints import etl_jtl_import

api_router = APIRouter()
api_router.include_router(etl_jtl_import.router, prefix="/etl/jtl", tags=["etl"])
