---
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  LM_PYTHON_VERSION: "2"
  DS_PYTHON_VERSION: "3"
  PIP_EXTRA_INDEX_URL: "https://pkgs.frkl.io/frkl/dev"

#include:
#  - template: Dependency-Scanning.gitlab-ci.yml
#  - template: License-Management.gitlab-ci.yml

image: python:3.7

stages:
  - test
  - release
  - push

python37:
  image: python:3.7
  stage: test
  before_script:
    - curl -O https://bootstrap.pypa.io/get-pip.py
    - python get-pip.py
    - pip install -U setuptools
  script:
    - pip install tox
    - tox -e py37

#python38:
#  image: python:3.8
#  stage: test
#  before_script:
#    - curl -O https://bootstrap.pypa.io/get-pip.py
#    - python get-pip.py
#    - pip install -U setuptools
#  script:
#    - pip install tox
#    - tox -e py38

pages:
  before_script:
    - "pip install --extra-index-url https://pkgs.frkl.io/frkl/dev -r requirements-docs.txt"
    - "pip install --extra-index-url https://pkgs.frkl.io/frkl/dev ."
  script:
    - portray as_html -o public
  artifacts:
    paths:
    - public

flake8:
  image: python:3.7
  stage: test
  before_script:
    - curl -O https://bootstrap.pypa.io/get-pip.py
    - python get-pip.py
    - pip install -U setuptools
  script:
    - pip install tox
    - tox -e flake8

commitlint:
  stage: test
  image: node:8
  only:
    - master
    - merge_requests
    - develop
  script:
    - npm install -g @commitlint/cli @commitlint/config-conventional
    - export LAST_TAG=$(git describe --abbrev=0 --tags) # get last tag
    - commitlint --from=$LAST_TAG   # check commit convention from last tag

build:
  stage: release
  only:
    - develop
  script:
    - pip install devpi-client
    - rm -rf dist build
    - devpi use https://pkgs.frkl.io/
    - devpi login frkl --password="${DEVPI_PASSWORD}"
    - devpi use /frkl/dev
    - devpi upload
  artifacts:
    paths:
      - dist/
    expire_in: 1 month

build_stable:
  stage: release
  only:
    - /^\d+\.\d+\.\d+$/
  except:
    - branches
  script:
    - pip install devpi-client
    - rm -rf dist build
    - devpi use https://pkgs.frkl.io/
    - devpi login frkl --password="${DEVPI_PASSWORD}"
    - devpi use /frkl/stable
    - devpi upload
    - UPLOAD_FILE=(dist/freckles-*.tar.gz)
    - "curl -F package=@${UPLOAD_FILE} https://${GEMFURY_TOKEN}@push.fury.io/frkl/"
    - UPLOAD_FILE=(dist/freckles-*.whl)
    - "curl -F package=@${UPLOAD_FILE} https://${GEMFURY_TOKEN}@push.fury.io/frkl/"
  artifacts:
    paths:
      - dist/
    expire_in: 1 month

build_beta:
  stage: release
  only:
    - /^\d+\.\d+\.\d+b\d+$/
  except:
    - branches
  script:
    - pip install devpi-client
    - rm -rf dist build
    - devpi use https://pkgs.frkl.io/
    - devpi login frkl --password="${DEVPI_PASSWORD}"
    - devpi use /frkl/beta
    - devpi upload
    - UPLOAD_FILE=(dist/freckles-*.tar.gz)
    - "curl -F package=@${UPLOAD_FILE} https://${GEMFURY_TOKEN}@push.fury.io/frkl/"
    - UPLOAD_FILE=(dist/freckles-*.whl)
    - "curl -F package=@${UPLOAD_FILE} https://${GEMFURY_TOKEN}@push.fury.io/frkl/"
  artifacts:
    paths:
      - dist/
    expire_in: 1 month

container_image_dev:
   stage: release
   image: docker:19
   only:
     - develop
   environment:
     name: dev
   tags:
     - shell
   script:
     - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
     # fetches the latest image (not failing if image is not found)
     - docker pull $CI_REGISTRY/retailiate/retailiate-api:dev || true
     # builds the project, passing proxy variables, and vcs vars for LABEL
     # notice the cache-from, which is going to use the image we just pulled locally
     # the built image is tagged locally with the commit SHA, and then pushed to
     # the GitLab registry
     - >
       docker build
       --pull
       --build-arg http_proxy=$http_proxy
       --build-arg https_proxy=$https_proxy
       --build-arg no_proxy=$no_proxy
       --build-arg VCS_REF=$CI_COMMIT_SHA
       --build-arg VCS_URL=$CI_PROJECT_URL
       --cache-from $CI_REGISTRY/retailiate/retailiate-api:dev
       --tag $CI_REGISTRY/retailiate/retailiate-api:dev
       .
     - docker push $CI_REGISTRY/retailiate/retailiate-api:dev

push_latest_image:
   variables:
     # We are just playing with Docker here.
     # We do not need GitLab to clone the source code.
     GIT_STRATEGY: none
   stage: push
   tags:
     - shell
   only:
     variables:
       - $STABLE == "true"
#   tags:
#     - shell
   script:
     - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
     # Because we have no guarantee that this job will be picked up by the same runner
     # that built the image in the previous step, we pull it again locally
     - docker pull $CI_REGISTRY/retailiate/retailiate-core:$CI_COMMIT_SHA
     # Then we tag it "latest"
     - docker tag $CI_REGISTRY/retailiate/retailiate-core:$CI_COMMIT_SHA $CI_REGISTRY/retailiate/retailiate-core:latest
     # Annnd we push it.
     - docker push $CI_REGISTRY/retailiate/retailiate-core:latest
